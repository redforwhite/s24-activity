console.log("HEllO po sa inyong lahat");


let number = 69;
let getCube = number ** 3;

console.log(`The cube of ${number} is ${getCube}!`);

let address = [96, "BGC Highstreet", "Zimbabwe", 8000];

const [houseNumber, street, state, zipCode] = address;
console.log(`I live at ${houseNumber} ${street}, ${state} ${zipCode}.`);

let animal = {
	givenName: "Manang",
	specie: "Unimais",
	weight: 108,
	measurement: 9991,
};

const {givenName, specie, weight, measurement} = animal
console.log(`${givenName} was a ${specie}. He weighed at ${weight} kgs with a measurement of ${measurement} mm.`);

let numbers = [1, 2, 3, 57, 80];

numbers.forEach((num) => {
	console.log(num);
});

let reduceNumber = numbers.reduce((x, y) => x + y);

console.log(reduceNumber);


class Dog {
	constructor(name, age, breed){
		this.name = name;
		this.age = age;
		this.breed = breed;

	}
}

const myDog = new Dog("Snoop Dog", 45, "Alaskan Pit Chiwawa");
console.log(myDog);